const express = require('express');
const app = express();
const {sequelize} = require('./models');
const errorHandler = require('./middlewares/errorHandler');
const appErrorHandler = require('./middlewares/appErrorHandler');
const morgan = require('morgan');
const chalk = require('chalk');
const path = require('path');
const {fork} = require('child_process');
const session = require('express-session');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const SequelizeStore = require('connect-session-sequelize')(session.Store);
const bodyParser = require('body-parser');

// Limit to file uploads
app.use(bodyParser.json({limit: '300mb'}));
app.use(bodyParser.urlencoded({limit: '300mb', extended: true}));

// session store
const sessionStore = new SequelizeStore({
    db: sequelize
});
app.use(morgan('dev'));
// session
app.use(
    session({
        secret: 'secret strategic notice board code',
        name: 'noticeBoard',
        proxy: true,
        resave: true,
        store: sessionStore,
        saveUninitialized: false,
        cookie: {
            path: '/',
            httpOnly: true,
            secure: false,
            maxAge: 1000
        }
    })
);
sessionStore.sync();

const authRouter = require('./routes/auth');
const bookRouter = require('./routes/book');
const searchRouter = require('./routes/search');
const userRouter = require('./routes/user');
const subjectRouter = require('./routes/subject');

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE,PATCH");
    res.header("Access-Control-Allow-Headers", "*");
    next();
});
app.use(express.static(path.join(__dirname, 'public')));

app.use('/auth', authRouter);
app.use('/book', bookRouter);
app.use('/search', searchRouter);
app.use('/user', userRouter);
app.use('/subject', subjectRouter);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(appErrorHandler);

/**
 * This child process using for check is user return book to office
 */
(() => {
    const isBookReturned = fork(path.resolve('./microservice/bookChecker'));
    isBookReturned.send('start');

    console.log(chalk.green('Child process started !'));
})();

module.exports = app;