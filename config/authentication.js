module.exports = {
  secretAccess: 'super secret secret key',
  secretRefresh: 'd,mawiodawndiuabdbfawubduav',
  hashAlgorithm: 'sha256',
  algorithm: 'HS256',
  ttlAccess: 15 * 60,
  ttlRefresh: 7 * 60 * 60,
  getSecret() {
    return Buffer.from(this.secretAccess).toString('base64');
  },
  getSecretRefresh() {
    return Buffer.from(this.secretRefresh).toString('base64');
  }

};
