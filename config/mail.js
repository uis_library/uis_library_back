module.exports = Object.freeze({
    "HR_EMAIL":              process.env.MAIL_FROM_ADDRESS || "hr@ukrinsoft.com",
    "NOTIFICATION_EMAIL":    process.env.MAIL_FROM_ADDRESS || 'hr@ukrinsoft.com',
    "NOTIFICATION_PASSWORD": process.env.MAIL_PASSWORD || "NO_PASS",
    "EMAIL_HOST":            process.env.MAIL_HOST || "mail.smtp2go.com",
    "EMAIL_PORT":            process.env.MAIL_PORT || "2525"
});