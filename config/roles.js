module.exports = {
  ADMINISTRATOR: 'administrator',
  MODERATOR: 'moderator',
  CUSTOMER: 'customer',
  CANDIDATE: 'candidate',
  EMPLOYEE: 'employee'
};
