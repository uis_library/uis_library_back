'use strict';
const jwt = require('jsonwebtoken');

/**
 * Create token.
 * @param {Object} payload
 * @param {String} secretKey
 * @param {Object} headers
 * @returns {Promise}
 * @throw {Error}
 */
const createToken = (payload, secretKey, headers) => {
  return new Promise((resolve, reject) => {
    if (!(
      typeof payload === 'object'
      && typeof secretKey === 'string'
      && typeof headers === 'object'
      && headers.hasOwnProperty('algorithm')
      && headers.hasOwnProperty('expiresIn')
      && (typeof headers.expiresIn === 'string' || typeof headers.expiresIn === 'number')
    )) {
      return reject(new Error('Invalid parameters'));
    }
    // here we create token
    const token = jwt.sign(payload, secretKey, headers);

    return resolve(token);
  })
};
exports.createToken = createToken;

/**
 * Verify token.
 * @param {String} token
 * @param {String} secretKey
 * @returns {Promise}
 */
const verifyToken = (token, secretKey) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secretKey, (err, decodedToken) => {
      if (err) return reject(err);

      return resolve(decodedToken);
    })
  })
};
exports.verifyToken = verifyToken;
