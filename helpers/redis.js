const redis = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient({
  db: 0,
});

client.on('error', (err) => {
  console.error(`
____________REDIS ERROR____________
    ${err.stack}
___________________________________
    `);
process.exit(1);
})
;

module.exports = client;
