const chalk = require('chalk');
const db = require('../../models/index');
const {FullSearch, DigitalInfo, Subject} = db.sequelize.models;
const {TEXT, AUDIO, VIDEO} = require('../../constants/files');

/**
 * This method insert all information of book in table FullSearch
 * I get all info about book. Creating one long string of this info
 * Convert this string to lower case and insert into table
 * @param id
 * @param bookInfo
 * @param fileInfo
 * @returns {Promise<void>}
 */
module.exports = async (id, bookInfo, fileInfo) => {
  let typeOfFile = '';
  let typeOfContent = '';
  const searchString = [];
  const {type_of_book} = bookInfo;

  if (fileInfo && (type_of_book === 'digital' || type_of_book === 'audio')) {
    const fileName = `/files/${fileInfo.filename}`;
    typeOfFile = fileInfo.filename.split('.').pop();
    if (TEXT.includes(fileInfo.mimetype)) typeOfContent = 'text';
    if (AUDIO.includes(fileInfo.mimetype)) typeOfContent = 'audio';

    searchString.push(typeOfFile, typeOfContent);

    await DigitalInfo.create({
      book_id: id,
      type_of_file: typeOfFile,
      location: fileName,
      type_of_content: typeOfContent
    });

    console.log(chalk.bgYellow.magenta('DIGITAL INFO INSERTED'))
  }

  // String to search by one field
  for (let i in bookInfo){
    if(bookInfo.hasOwnProperty(`${i}`)) searchString.push(bookInfo[i])
  }

  await FullSearch.create({
    book_id: id,
    description: searchString.join(' ').toLowerCase()
  });
  console.log(chalk.bgYellow.magenta('FULL SEARCH INSERTED'))
};
