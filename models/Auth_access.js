module.exports = (sequelize, DataTypes) => {
  const Auth_access = sequelize.define('Auth_access', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    ttl: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    timestamps: false,
    tableName: 'authAccess'
  });

  const User = sequelize.import('./User.js');
  Auth_access.belongsTo(User, {foreignKey: 'userId'});

  return Auth_access;
};