module.exports = (sequelize, DataTypes) => {
  const Auth_refresh = sequelize.define('Auth_refresh', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    ttl: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    timestamps: false,
    tableName: 'authRefresh'
  });

  const User = sequelize.import('./User.js');
  Auth_refresh.belongsTo(User, {foreignKey: 'userId'});

  return Auth_refresh;
};