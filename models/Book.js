'use strict';

module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('Book', {
     id: {
       type: DataTypes.INTEGER,
       primaryKey: true,
       autoIncrement: true
     },
     title: {
       type: DataTypes.STRING,
       allowNull: false,
     },
     author: {
       type: DataTypes.STRING,
       allowNull: false
     },
     type_of_book: {
       // type: DataTypes.BOOLEAN,
       type: DataTypes.ENUM(['digital', 'alive', 'audio']),
       allowNull: false,
     },
     description: {
       type: DataTypes.TEXT
     },
     tags: {
       type: DataTypes.TEXT
     },
     subject_id: {
       type: DataTypes.INTEGER,
       foreignKey: true
     },
     user_id: {
       type: DataTypes.INTEGER,
       foreignKey: true,
       allowNull: false
     },
     publisher: {
       type: DataTypes.STRING
     },
     image: {
       type: DataTypes.TEXT
     },
     barcode: {
       type: DataTypes.INTEGER
     },
     is_reading: {
       type: DataTypes.BOOLEAN,
     },
     created_at: {
       type: DataTypes.DATE,
       allowNull: false,
       defaultValue: new Date().toISOString()
     }
   },
   {
     tableName: 'book',
     timestamps: false
   });

  const User = sequelize.import('./User.js');
  const Subject = sequelize.import('./Subject.js');
  Book.belongsTo(Subject, {foreignKey: 'subject_id'});
  Book.belongsTo(User, {foreignKey: 'user_id'});
  return Book
};
