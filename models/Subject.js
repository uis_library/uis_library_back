'use strict';
module.exports = (sequelize, DataTypes) => {
    const Subject = sequelize.define('Subject', {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            title: {
                type: DataTypes.STRING,
            }
        },
        {
            tableName: 'subject',
            timestamps: false
        });
    return Subject
};