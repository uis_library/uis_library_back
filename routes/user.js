const router = require('express').Router();
const ensureAuthenticated = require('../middlewares/ensureAuthenticated');
const permit = require('../middlewares/permission');
const {ADMINISTRATOR, EMPLOYEE, MODERATOR} = require('../config/roles');
const userController = require('../controllers/user');

router.get('/info', ensureAuthenticated, userController.getInfo);
router.get('/books', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), userController.getReadBook);

module.exports = router;