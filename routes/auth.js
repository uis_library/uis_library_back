const router = require('express').Router();
const ensureAuthenticated = require('../middlewares/ensureAuthenticated');

const authController = require('../controllers/auth');

router.post('/login', authController.login);
router.delete('/logout', ensureAuthenticated, authController.logout);
router.get('/token/refresh', authController.refreshToken);

module.exports = router;
