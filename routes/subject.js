const router = require('express').Router();
const ensureAuthenticated = require('../middlewares/ensureAuthenticated');
const permit = require('../middlewares/permission');
const {ADMINISTRATOR, EMPLOYEE, MODERATOR} = require('../config/roles');


const subjectController = require('../controllers/subject');

router.get('/', subjectController.getAllSubjects);
router.get('/:id/book', subjectController.getSubjectById);
router.post('/', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), subjectController.createSubject);
router.put('/:id', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), subjectController.updateById);
router.delete('/:id', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), subjectController.removeById);

module.exports = router;