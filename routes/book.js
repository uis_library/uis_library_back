const router = require('express').Router();
const ensureAuthenticated = require('../middlewares/ensureAuthenticated');
const permit = require('../middlewares/permission');
const {ADMINISTRATOR, EMPLOYEE, MODERATOR, CUSTOMER, CANDIDATE} = require('../config/roles');
const upload = require('../middlewares/uploadFile');
const activityController = require('../controllers/activity');
const bookController = require('../controllers/book');
const commentController = require('../controllers/comment');
const ratingController = require('../controllers/rating');

router.get('/:id/activity/reading', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), activityController.getReadingActivityByBookId);
router.get('/:id/activity/rating', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), activityController.getRatingActivityByBookId);
router.get('/:id/activity/comment', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), activityController.getCommentActivityByBookId);
router.get('/activity/topBooks', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), activityController.getMostReadBooks);
router.get('/activity/fullReading', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), activityController.getFullReadingActivity);
router.get('/activity/fullComment', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), activityController.getFullCommentActivity);
router.get('/activity/fullRating', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), activityController.getFullRatingActivity);
router.get('/activity/topUsers', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), activityController.getTopUsers);

router.get('/:id/comment', ensureAuthenticated, commentController.getAllBookComment);
router.post('/:id/comment', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), commentController.createNewComment);
router.put('/comment/:id', ensureAuthenticated, commentController.updateById);
router.delete('/comment/:id', ensureAuthenticated, commentController.deleteById);

router.get('/:id/rating', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), ratingController.getBookRating);
router.post('/:id/rating', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), ratingController.rateBook);
router.delete('/:id/rating', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), ratingController.deleteVote);

router.get('/topByRating', bookController.getTopByRating);
router.get('/topByComments', bookController.getTopByComments);
router.get('/topByReading', bookController.getTopByReading);
router.get('/:id', bookController.getBookById);
router.get('/:id/download', ensureAuthenticated, permit(CANDIDATE, CUSTOMER, EMPLOYEE, ADMINISTRATOR), bookController.downloadBook);
router.post('/', ensureAuthenticated, upload.fields([{name: 'photo', maxCount: 1}, {
  name: 'file',
  maxCount: 1
}]), bookController.addBook);
router.get('/', bookController.getAllBooks);
router.get('/:id/read', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), bookController.takeBookForReadingById);
router.get('/read/:barcode', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), bookController.takeBookForReadingByBarcode);
router.patch('/:id', ensureAuthenticated, bookController.continueReading);//?????????????
router.delete('/:id/return', ensureAuthenticated, bookController.returnBookById);
router.delete('/return/:barcode', ensureAuthenticated, bookController.returnBookByBarcode);
//ADMIN ROUTES
router.delete('/:id', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR), bookController.deleteBook);
router.put('/:id', ensureAuthenticated, permit(ADMINISTRATOR, MODERATOR, EMPLOYEE), upload.fields([{
  name: 'photo',
  maxCount: 1
}, {name: 'file', maxCount: 1}]), bookController.updateBook);

module.exports = router;