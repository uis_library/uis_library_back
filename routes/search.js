const router = require('express').Router();

let searchController = require('../controllers/search');

router.get('/tag/:tag', searchController.searchByTag);
router.get('/', searchController.fullSearch);

module.exports = router;