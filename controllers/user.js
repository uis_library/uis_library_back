const db = require('../models/index');
const {User, ReadingActivity, Book, Subject, Rating} = db.sequelize.models;
const Sequelize = require("sequelize");

class UserController {
  async getInfo(req, res, next) {
    try {
      const {id} = req.auth;
      const user = await User.findByPk(id);
      res.json({
        success: true,
        user
      })
    } catch (error) {
      next(error)
    }
  };

  async getReadBook(req, res, next) {
    try {
      const {id: userId} = req.auth;

      const bookInfo = await ReadingActivity.findAll({
        where: {
          user_id: userId,
          get_back: true
        }
      });

      const bookIds = bookInfo.map(book => book.book_id);

      const allReadBooks = await Book.findAll({
        where: {
          id: bookIds
        },
        include: [
          {
            model: Subject,
            attributes: ['title']
          }
        ]
      });
      // SELECT bookid, AVG(star), COUNT(id) FROM rating GROUP BY bookid ORDER BY AVG(star) DESC
      const ratingInfo = await Rating.findAll({
        attributes: [
          'book_id',
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: 'book_id',
        where: {
          book_id: bookIds
        }
      });

      const books = allReadBooks.map(bookStat => {
        const rating = ratingInfo.find(rating => bookStat.id === rating.book_id);
        if (!rating) return bookStat;

        bookStat.dataValues.countOfVotes = +rating.dataValues.countOfVotes;
        bookStat.dataValues.avgStar = +(rating.dataValues.avgStar.slice(0, 3));
        return bookStat;
      });

      res.json({
        success: true,
        books
      });
    } catch (error) {
      next(error);
    }
  };
}

module.exports = new UserController();