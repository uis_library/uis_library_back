const db = require('../models/index');
const {Book, Rating, ReadingActivity, CommentActivity, User, Comment} = db.sequelize.models;
const {RequestError} = require('../errors');
const Sequelize = require("sequelize");


class Activity {
    constructor() {
        this.getCommentActivityByBookId = this.getCommentActivityByBookId.bind(this);
        this.getFullCommentActivity = this.getFullCommentActivity.bind(this);
        this.getFullRatingActivity = this.getFullRatingActivity.bind(this);
        this.getFullReadingActivity = this.getFullReadingActivity.bind(this);
        this.getMostReadBooks = this.getMostReadBooks.bind(this);
        this.getTopUsers = this.getTopUsers.bind(this);
        this.getRatingActivityByBookId = this.getRatingActivityByBookId.bind(this);
        this.getReadingActivityByBookId = this.getReadingActivityByBookId.bind(this);
    }

    /**
     * This method using for get all comment of book
     * We get info about add new comment and update old comments.
     * @param req
     * @param res
     * @param next
     * @returns {Promise<void>}
     */
    async getCommentActivityByBookId(req, res, next) {
        try {
            const bookId = Number(req.params.id);
            if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with id');

            const commentActivity = await Comment.findAll({
                where: {
                    book_id: bookId
                },
                include: [{
                    model: User,
                    attributes: ['name']
                }],
                order: [['created_at', 'DESC']]
            });

            res.json({
                success: true,
                message: commentActivity
            })
        } catch (error) {
            next(error);
        }
    }

    /**
     * This method for search rating history of book.
     * Get BookId from params req.param and search all rating of this book
     * @param req
     * @param res
     * @param next
     * @returns {Promise<void>}
     */
    async getRatingActivityByBookId(req, res, next) {
        try {
            const bookId = Number(req.params.id);
            if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with id');

            const ratingActivity = await Rating.findAll({
                where: {
                    book_id: bookId
                },
                include: [{
                    model: User,
                    attributes: ['name']
                }],
                order: [['created_at', 'DESC']]
            });

            res.json({
                success: true,
                message: ratingActivity
            })
        } catch (error) {
            next(error)
        }
    };

    async getReadingActivityByBookId(req, res, next) {
        try {
            const bookId = Number(req.params.id);
            if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with id');

            const readingActivity = await ReadingActivity.findAll({
                where: {
                    book_id: bookId
                },
                include: [{
                    model: User,
                    attributes: ['name']
                }],
                order: [['created_at', 'DESC']]
            });

            res.json({
                success: true,
                message: readingActivity
            })
        } catch (error) {
            next(error)
        }
    };

    /**
     * Method for get all comment activity.
     * When we select info from data base we join it to book and user model
     * For get full info about action
     * @returns {Promise<void>}
     * @param req
     * @param res
     * @param next
     */

    async getFullCommentActivity(req, res, next) {
        try {
            const commentActivity = await CommentActivity.findAll({
                include: [{
                    model: User,
                    attributes: ['name']
                }, {
                    model: Book,
                    attributes: ['title']
                }],
                order: [['created_at', 'DESC']]
            });

            res.json({
                success: true,
                message: commentActivity
            })
        } catch (error) {
            next(error);
        }
    }

    /**
     * Method for get all rating activity.
     * When we select info from data base we join it to book and user model
     * For get full info about action
     * @returns {Promise<void>}
     * @param req
     * @param res
     * @param next
     */
    async getFullRatingActivity(req, res, next) {
        try {
            const rating = await Rating.findAll({
                include: [
                    {
                        model: User,
                        attributes: ['name']
                    },
                    {
                        model: Book,
                        attributes: ['title']
                    }
                ],
                order: [['created_at', 'DESC']]
            });

            res.json({
                success: true,
                message: rating
            })
        } catch (error) {
            next(error);
        }
    }

    /**
     * Method for get all book activity.
     * We know when user get book for reading, continue read and back it to office
     * When we select info from data base we join it to book and user model
     * For get full info about action
     * @returns {Promise<void>}
     * @param req
     * @param res
     * @param next
     */
    async getFullReadingActivity(req, res, next) {
        try {
            const readingActivity = await ReadingActivity.findAll({
                include: [
                    {
                        model: User,
                        attributes: ['name']
                    },
                    {
                        model: Book,
                        attributes: ['title']
                    }
                ],
                order: [['created_at', 'DESC']]
            });
            res.json({
                success: true,
                message: readingActivity
            })
        } catch (error) {
            next(e);
        }
    }

    /**
     * In this method we get top books for reading count.
     * @param req
     * @param res
     * @param next
     * @returns {Promise<void>}
     */
    async getMostReadBooks(req, res, next) {
        try {
            const readingActivity = await ReadingActivity.findAll({
                attributes: [
                    [Sequelize.fn('COUNT', Sequelize.col('ReadingActivity.id')), 'countOfRead']
                ],
                include: [{
                    model: Book,
                    attributes: ['title', "id", "is_digital"]
                }],
                group: ['ReadingActivity.book_id', "Book.id"],
                order: [[Sequelize.fn('COUNT', Sequelize.col('ReadingActivity.id')), 'DESC']],
            });
            res.json({
                success: true,
                message: readingActivity
            })
        } catch (error) {
            next(error);
        }
    };

    async getTopUsers(req, res, next) {
        try {
            const readingActivity = await ReadingActivity.findAll({
                attributes: [
                    [Sequelize.fn('COUNT', Sequelize.col('ReadingActivity.id')), 'countOfRead']
                ],
                include: [{
                    model: User,
                    attributes: ['name']
                }],
                group: ["User.id"],
                order: [[Sequelize.fn('COUNT', Sequelize.col('ReadingActivity.id')), 'DESC']],
            });
            res.json({
                success: true,
                message: readingActivity
            })
        } catch (error) {
            next(error)
        }
    };
}
module.exports = new Activity();