const db = require('../models/index');
const {Rating} = db.sequelize.models;
const {RequestError,ConflictError} = require('../errors');
const Sequelize = require("sequelize");

class RatingController {
  constructor() {
    this.deleteVote = this.deleteVote.bind(this);
    this.rateBook = this.rateBook.bind(this);
    this.getBookRating = this.getBookRating.bind(this)
  }

  async getBookRating(req, res, next) {
    try {
      const bookId = Number(req.params.id);
      let votedStar;
      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with book id');

      const voteInfo = await Rating.findAll({
        attributes: ['user_id', "star"],
        where: {
          book_id: bookId
        }
      });
      const {id: userId} = req.auth;
      // If user ID not found in voted array - he can vote
      let isUserCanVote = !voteInfo.some(elem => elem.user_id === userId);
      // If user already voted. Get star what he rate
      if (!isUserCanVote) {
        let index = voteInfo.map(el => el.user_id).indexOf(userId);
        votedStar = voteInfo[index].star
      }

      const [rating] = await Rating.findAll({
        attributes: [
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: "book_id",
        where: {
          book_id: bookId
        },
      });

      if (!rating) throw new ConflictError('No one rated the book!');


      rating.dataValues.isUserCanVote = isUserCanVote;
      rating.dataValues.votedStar = votedStar;
      rating.dataValues.countOfVotes = +rating.dataValues.countOfVotes;
      rating.dataValues.avgStar = +(rating.dataValues.avgStar.slice(0, 3));
      res.json({
        success: true,
        rating
      })
    } catch (error) {
      next(error)
    }
  };

  async rateBook(req, res, next) {
    try {
      const {star} = req.body;
      const bookId = Number(req.params.id);
      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with id');
      if (!star) throw new Error('Body is not correct');
      const {id: user_id} = req.auth;
      const userIds = await Rating.findAll({
        attributes: ['user_id'],
        where: {
          book_id: bookId
        }
      });

      const isUserAlreadyVote = userIds.some(elem => elem.user_id === user_id);
      if (isUserAlreadyVote) throw new ConflictError('You already voted');

      await Rating.create({
        user_id,
        book_id: bookId,
        star,
        created_at: new Date().toISOString()
      });
      res.json({
        success: true
      })
    } catch (error) {
      next(error)
    }
  };

  async deleteVote(req, res, next) {
    try {
      const bookId = Number(req.params.id);
      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with id');
      const {id: user_id} = req.auth;
      const rating = Rating.findOne({
        where: {
          user_id,
          book_id: bookId
        }
      });
      if (!rating) throw new ConflictError('Your vote is absent');

      await rating.destroy();
      res.json({
        success: true,
      })
    } catch (error) {
      next(error)
    }
  };
}

module.exports = new RatingController();