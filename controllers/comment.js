const db = require('../models/index');
const {User, Comment, CommentActivity, Book} = db.sequelize.models;
const {ADMIN_ROLES} = require('../constants/values');
const {RequestError, ConflictError} = require('../errors');

class Comments {
    constructor() {
        this.createNewComment = this.createNewComment.bind(this);
        this.deleteById = this.deleteById.bind(this);
        this.getAllBookComment = this.getAllBookComment.bind(this);
        this.updateById = this.updateById.bind(this)
    }

    async createNewComment(req, res, next) {
        try {
            const bookId = Number(req.params.id);
            if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with book id');
            const {comment} = req.body;
            const {id: userId} = req.auth;

            const book = await Book.findByPk(bookId);
            if (!book) throw new ConflictError('The book is unavailable.');

            const createdComment = await Comment.create({
                user_id: userId,
                created_at: new Date().toISOString(),
                comment,
                book_id: bookId
            });
            const {id: commentId} = createdComment;
            await CommentActivity.create({
                book_id: bookId,
                comment_id: commentId,
                user_id: userId,
                is_create: true,
                old_comment: '',
                new_comment: comment,
                created_at: new Date().toISOString()
            });
            res.json({
                success: true
            })
        } catch (error) {
            next(error)
        }
    };

    async deleteById(req, res, next) {
        try {
            const commentId = Number(req.params.id);
            if (typeof commentId !== 'number' || isNaN(commentId) || commentId <= 0) throw new RequestError('Bad request');
            const {id: userId, role} = req.auth;
            const comment = await Comment.findByPk(commentId);
            if (!comment) throw new Error('Comment not found');

            const {user_id} = comment;
            if (!ADMIN_ROLES.includes(role) && userId !== user_id) throw new ConflictError('You are not admin');
            await CommentActivity.destroy({
                where: {
                    comment_id: commentId
                }
            });
            await comment.destroy();
            res.json({
                success: true,
            })
        } catch (error) {
            next(error)
        }
    };

    async getAllBookComment(req, res, next) {
        try {
            // const limit = req.query.hasOwnProperty('limit') ? Number(req.query.limit) : 10;
            const bookId = Number(req.params.id);
            if (!bookId || typeof bookId !== 'number' || isNaN(bookId)) throw new RequestError('Bad request, problem with book id');
            // if (typeof limit !== 'number' || isNaN(limit) || limit < 0) throw new ConflictError('Incorrect parameter limit');
            // Include makes a SQL JOIN on the table in scopes.
            // By what fields we doing a search we define in ORM model
            const comments = await Comment.findAll({
                where: {
                    book_id: bookId
                },
                order: [["created_at", 'DESC']],
                include: [User],
                // limit
            });
            res.json({
                success: true,
                comments
            })
        } catch (error) {
            next(error);
        }
    };

    async updateById(req, res, next) {
        try {
            const commentId = Number(req.params.id);
            const {comment: newComment} = req.body;
            const {id: userId} = req.auth;
            if (!commentId) throw new RequestError('Bad request');
            if (!newComment) throw new ConflictError('No comment found');

            const comment = await Comment.findByPk(commentId);
            if (!comment) throw new Error('Comment not found');
            if (userId !== comment.user_id) throw new ConflictError('You have not permission');
            const {comment: oldComment, book_id} = comment;

            await comment.update({
                comment: newComment
            });
            // Insert record into statistic activity table
            await CommentActivity.create({
                book_id,
                comment_id: commentId,
                user_id: userId,
                is_update: true,
                old_comment: oldComment,
                new_comment: newComment,
                created_at: new Date().toISOString()
            });
            res.json({
                success: true,
            })
        } catch (error) {
            next(error)
        }
    };
}
module.exports = new Comments();