const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const db = require('../models/index');
const {FullSearch, Book, Rating, Subject} = db.sequelize.models;

class SearchController {
  constructor() {
    this.fullSearch = this.fullSearch.bind(this);
    this.searchByTag = this.searchByTag.bind(this)
  }

  /**
   * This method using just for searching bu one field.
   * Best practice will be using something like elastic search.
   * But this method is useful too.
   * I have table with 2 fields. First field is book Id.
   * Second is all book subscription.
   * I search by this one field, and have book id.
   * Then i find book and rating bu this id
   * @param req
   * @param res
   * @param next
   * @returns {Promise<void>}
   */
  async fullSearch(req, res, next) {
    try {
      const keyWord = req.query.search;
      if (!keyWord) throw new Error('Something wrong with URL');

      const booksByKeyWord = await FullSearch.findAll({
        attributes: ['book_id'],
        where: {
          description: {
            [Op.like]: `%${keyWord.toLowerCase()}%`
          }
        },
        include: [
          {
            model: Subject,
            attributes: ['title']
          }
        ]
      });

      let booksIds = [];
      booksByKeyWord.forEach(book => booksIds.push(book.book_id));

      const books = await Book.findAll({
        where: {
          id: booksIds
        }
      });

      const booksInfo = await Rating.findAll({
        attributes: [
          'book_id',
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: 'book_id',
        where: {
          book_id: booksIds
        }
      });

      const searchedBooks = books.map(bookStat => {
        const rating = booksInfo.find(rating => bookStat.id === rating.book_id);
        if (!rating) return bookStat;

        bookStat.dataValues.countOfVotes = +rating.dataValues.countOfVotes;
        bookStat.dataValues.avgStar = +(rating.dataValues.avgStar.slice(0, 3));
        return bookStat;
      });

      res.json({
        success: true,
        message: searchedBooks
      })
    } catch (error) {
      next(error)
    }
  };

  /**
   * This method using for searching by tag from.
   * We have tag in URL and search in book table by this tag
   * @param req
   * @param res
   * @param next
   * @returns {Promise<void>}
   */
  async searchByTag(req, res, next) {
    try {
      const tag = req.params.tag.toLowerCase();
      if (!tag) throw new Error('Enter tag first');

      const bookByTag = await Book.findAll({
        where: {
          tags: {
            [Op.like]: `%${tag}%`
          }
        },
        include: [
          {
            model: Subject,
            attributes: ['title']
          }
        ]
      });

      if (!bookByTag) res.json({success: true, message: bookByTag});
      let bookIds = [];
      bookByTag.forEach(value => bookIds.push(value.id));
      const booksInfo = await Rating.findAll({
        attributes: [
          'book_id',
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: 'book_id',
        order: [[Sequelize.fn('AVG', Sequelize.col('star')), 'DESC']],
        where: {
          book_id: bookIds
        }
      });

      const searchedBooks = bookByTag.map(bookStat => {
        const rating = booksInfo.find(rating => bookStat.id === rating.book_id);
        if (!rating) return bookStat;

        bookStat.dataValues.countOfVotes = +rating.dataValues.countOfVotes;
        bookStat.dataValues.avgStar = +(rating.dataValues.avgStar.slice(0, 3));
        return bookStat;
      });

      res.json({
        success: true,
        message: searchedBooks
      })
    } catch (error) {
      next(error)
    }
  };

}

module.exports = new SearchController();