const db = require('../models/index');
const {User, Auth_access, Auth_refresh} = db.sequelize.models;
const Joi = require('joi');
const getUserInfoFromHR = require('../helpers/getUserInfoFromHR');
const {verifyToken, createToken} = require('../helpers/token');
const authConfig = require('../config/authentication');
const {AuthError} = require('../errors');

class Auth {
  constructor() {
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.refreshToken = this.refreshToken.bind(this)
  }

  async login(req, res, next) {
    try {
      const schema = Joi.object().keys({
        email: Joi.string().trim().email().required(),
        password: Joi.string().trim().required().error(new Error('Password is not valid, please try again.')),
      });
      const {email, password} = await Joi.validate(req.body, schema, {abortEarly: false});
      const userInfo = await getUserInfoFromHR(email, password);

      if (!userInfo) throw new Error('User was not found');

      let user = await User.findOne({
        where: {
          email: email,
          name: userInfo.name
        }
      });
      console.log(userInfo);

      if (!user) {
        user = await User.create({
          email,
          name: userInfo.name,
          role: userInfo.roles
        })
      }

      const {id, role} = user;
      const payload = {
        id,
        email,
        role,
      };

      const refreshToken = await createToken(payload, authConfig.getSecretRefresh(), {
        algorithm: authConfig.algorithm,
        expiresIn: authConfig.ttlRefresh,
      });

      const refreshTokenFromDB = await Auth_refresh.create({
        userId: user.id,
        token: refreshToken,
        ttl: Date.now() + authConfig.ttlRefresh * 1000
      });

      payload.refreshTokenId = refreshTokenFromDB.id;

      const accessToken = await createToken(payload, authConfig.getSecret(), {
        algorithm: authConfig.algorithm,
        expiresIn: authConfig.ttlAccess,
      });

      await Auth_access.create({
        userId: user.id,
        token: accessToken,
        ttl: Date.now() + authConfig.ttlAccess * 1000
      });

      res.json({
        success: true,
        message: {
          accessToken: `Bearer ${accessToken}`,
          refreshToken: `Bearer ${refreshToken}`,
        }
      })
    } catch (error) {
      next(error);
    }
  }

  async logout(req, res, next) {
    try {
      const {id, refreshTokenId} = req.auth;

      await Auth_access.destroy({
        where: {
          userId: id,
        }
      });
      await Auth_refresh.destroy({
        where: {
          id: refreshTokenId,
          userId: id
        }
      });

      res.json({
        success: true,
      })
    } catch (error) {
      next(error);
    }
  };

  async refreshToken(req, res, next) {
    try {
      if (!req.headers.hasOwnProperty('authorization')) throw new AuthError('Problem with refresh Token, it is missing.');

      const _refreshToken = req.headers.authorization;
      const [bearer, token] = _refreshToken.split(' ');

      if (!bearer || bearer !== 'Bearer' || !token) throw new AuthError('Token is not valid');

      const {
        id,
        role,
        username,
        email
      } = await verifyToken(token, authConfig.getSecretRefresh());
      const existRefreshToken = await Auth_refresh.findOne({
        where: {
          token
        }
      });

      if (!existRefreshToken) throw new AuthError('Problem with refresh Token, it is not valid.');

      await Auth_refresh.destroy({
        where: {
          userId: id,
          token,
        }
      });
      await Auth_access.destroy({
        where: {
          userId: id
        }
      });

      const payload = {
        id,
        role,
        email,
        username
      };


      const refreshToken = await createToken(payload, authConfig.getSecretRefresh(), {
        algorithm: authConfig.algorithm,
        expiresIn: authConfig.ttlRefresh
      });
      const refreshTokenFromDB = await Auth_refresh.create({
        userId: id,
        token: refreshToken,
        ttl: Date.now() + authConfig.ttlRefresh * 1000
      });

      payload.refreshTokenId = refreshTokenFromDB.id;

      const accessToken = await createToken(payload, authConfig.getSecret(), {
        algorithm : authConfig.algorithm,
        expiresIn: authConfig.ttlAccess,
      });
      await Auth_access.create({
        userId: id,
        token: accessToken,
        ttl: Date.now() + authConfig.ttlAccess * 1000
      });

      res.json({
        success: true,
        message: {
          accessToken: `Bearer ${accessToken}`,
          refreshToken: `Bearer ${refreshToken}`
        }
      })
    } catch (error) {
      next(error)
    }
  }
}

module.exports = new Auth();
