const db = require('../models/index');
const {Subject, Book} = db.sequelize.models;
const {RequestError} = require('../errors');


class SubjectController {
    constructor() {
        this.createSubject = this.createSubject.bind(this);
        this.getAllSubjects = this.getAllSubjects.bind(this);
        this.removeById = this.removeById.bind(this);
        this.updateById = this.updateById.bind(this)
    }

    async updateById(req, res, next) {
        try {
            const subjectId = Number(req.params.id);
            if (typeof subjectId !== 'number' || isNaN(subjectId) || subjectId <= 0) throw new RequestError('Bad request, problem with id');
            const {title} = req.body;
            if (!title) throw new RequestError('Bad request, no body');

            const subjectUpdate = await Subject.findById(subjectId);
            if (!subjectUpdate) throw new Error('Subject not found');

            await subjectUpdate.update({
                title
            });

            res.json({
                success: true
            })
        } catch (error) {
            next(error);
        }
    };

    async removeById(req, res, next) {
        try {
            const subjectId = Number(req.params.id);
            if (typeof subjectId !== 'number' || isNaN(subjectId) || subjectId <= 0) throw new RequestError('Bad request');

            const subject = await Subject.findById(subjectId);
            if (!subject) throw new Error('Subject not found');

            await subject.destroy();
            res.json({
                success: true
            })
        } catch (error) {
            next(error);
        }
    };


    async getAllSubjects(req, res, next) {
        try {
            const subjects = await Subject.findAll({
                order: ['title']
            });
            res.json({
                success: true,
                subjects
            })
        } catch (error) {
            next(error);
        }
    };
    async getSubjectById(req, res, next){
        try {
            const subjectId = Number(req.params.id);
            const page = req.query.hasOwnProperty('page') ? Number(req.query.page) : 1;
            const limit = req.query.hasOwnProperty('limit') ? Number(req.query.limit) : 10;
            if (typeof subjectId !== 'number' || isNaN(subjectId) || subjectId <= 0) throw new RequestError('Bad request, problem with subject id');
            const offset = (page * limit) - limit;
            const subject = {};

            const Books = await Book.findAndCountAll({
                where: {
                    subject_id: subjectId
                },
                include: [{
                    model: Subject,
                    attributes: ['title']
                }],
                offset,
                limit
            });
            subject.total = Books.count;
            subject.offset = offset;
            subject.books = Books.rows;

            res.json({
                success: true,
                subject
            })
        } catch (error) {
            next(error)
        }

    }

    async createSubject(req, res, next) {
        try {
            const {title} = req.body;
            if (!title) throw new RequestError('Bad request, no body');

            await Subject.create({
                title
            });
            res.json({
                success: true
            })
        } catch (error) {
            next(error)
        }
    };

}

module.exports = new SubjectController();