const db = require('../models/index');
const {
  Book,
  Comment,
  BookStat,
  ReadingActivity,
  Rating,
  Subject,
  FullSearch,
  DigitalInfo,
  CommentActivity,
  User
} = db.sequelize.models;
const {ADMIN_ROLES, MAIN_PATH, MILLISECONDS_ID_DAY} = require('../constants/values');
const {ConflictError, RequestError} = require('../errors');
const fs = require('fs');
const fsPromises = fs.promises;
const path = require('path');
const Sequelize = require('sequelize');
const Joi = require('joi');
const {TEXT, AUDIO} = require('../constants/files');


let updateAuxiliary = require('../helpers/bookHelpers/updateAuxiliary');
const insertAuxiliary = require('../helpers/bookHelpers/insertAuxiliary');

class Books {
  constructor() {
    this.addBook = this.addBook.bind(this);
    this.continueReading = this.continueReading.bind(this);
    this.deleteBook = this.deleteBook.bind(this);
    this.downloadBook = this.downloadBook.bind(this);
    this.getAllBooks = this.getAllBooks.bind(this);
    this.getBookById = this.getBookById.bind(this);
    this.getTopByComments = this.getTopByComments.bind(this);
    this.getTopByRating = this.getTopByRating.bind(this);
    this.getTopByReading = this.getTopByReading.bind(this);
    this.returnBookById = this.returnBookById.bind(this);
    this.takeBookForReadingById = this.takeBookForReadingById.bind(this);
    this.updateBook = this.updateBook.bind(this);
    this.takeBookForReadingByBarcode = this.takeBookForReadingByBarcode.bind(this);
    this.returnBookByBarcode = this.returnBookByBarcode.bind(this)
  }

  async addBook(req, res, next) {
    try {
      const schema = Joi.object().keys({
        title: Joi.string().required(),
        author: Joi.string().required(),
        summary: Joi.string(),
        subject: Joi.string(),
        publisher: Joi.string(),
        tags: Joi.string(),
        typeOfBook: Joi.string().required(),
        barcode: Joi.string().min(3)
      });
      const {id: userId} = req.auth;
      const bookInfo = await Joi.validate(req.body, schema, {abortEarly: false});
      const {title, author, summary = '', subject = '', publisher = '', tags = '', typeOfBook, barcode = ''} = bookInfo;
      const isDigital = typeOfBook === 'digital' || typeOfBook === 'audio';


      let photo = '';
      let file = '';

      if (req.files && req.files.photo) photo = req.files.photo;
      if (req.files && req.files.file) file = req.files.file;

      const book = {
        title,
        author,
        type_of_book: typeOfBook,
        user_id: userId
      };

      console.log(req.files);

      if (isDigital && (typeof file === 'undefined' || typeof file === 'string')) {
        throw new RequestError('Bad request, you need change type of file or input file');
      }

      const [fileInfo = ''] = file;

      if (isDigital && !(TEXT.includes(fileInfo.mimetype) || AUDIO.includes(fileInfo.mimetype))) {
        throw new ConflictError('Bad file extension.');
      }

      if (photo) book.image = `/images/${photo[0].filename}`;
      if (summary) book.summary = summary;
      if (tags) book.tags = tags;
      if (publisher) book.publisher = publisher;
      if (!isDigital) book.barcode = barcode;
      if (subject) {
        let subjectExist = await Subject.findOne({
          where: {
            title: subject
          }
        });
        if (!subjectExist) {
          subjectExist = await Subject.create({
            title: subject
          })
        }
        book.subject_id = subjectExist.id;
      }

      const bookDB = await Book.create(book);
      //for book type of digital extra information

      await insertAuxiliary(bookDB.id, book, fileInfo);

      res.json({
        success: true,
        bookId: bookDB.id
      })
    } catch (error) {
      next(error)
    }
  };

  async continueReading(req, res, next) {
    try {
      const {id: userId} = req.auth;
      const bookId = Number(req.params.id);

      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with book id');

      const bookStat = await BookStat.findOne({
        where: {
          book_id: bookId,
          user_id: userId
        }
      });
      if (!bookStat) throw new ConflictError('Book was not found');

      const {back_time, is_delaying} = bookStat;
      if (is_delaying) throw new ConflictError('U already delay this book');

      const newDate = new Date(back_time).getTime() + 14 * MILLISECONDS_ID_DAY;
      await bookStat.update({
        back_time: newDate,
        is_delaying: true,
        notification_count: 0
      });
      // Insert record into statistic activity table
      await ReadingActivity.create({
        user_id: userId,
        book_id: bookId,
        continue_read: true,
        created_at: new Date().toISOString()
      });
      res.json({
        success: true
      })
    } catch (error) {
      next(error)
    }
  };

  async deleteBook(req, res, next) {
    try {
      const bookId = Number(req.params.id);
      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with book id');

      const bookToDelete = await Book.findByPk(bookId);
      if (!bookToDelete) throw new ConflictError(`Book with id ${bookId} not found`);
      const {image} = bookToDelete;
      //Starting delete all record;
      await Promise.all([
        FullSearch.destroy({
          where: {
            book_id: bookId
          }
        }),
        Rating.destroy({
          where: {
            book_id: bookId
          }
        }),
        BookStat.destroy({
          where: {
            book_id: bookId,
          }
        }),
        Comment.destroy({
          where: {
            book_id: bookId
          }
        }),
        ReadingActivity.destroy({
          where: {
            book_id: bookId
          }
        }),
        CommentActivity.destroy({
          where: {
            book_id: bookId
          }
        })
      ]);
      // Then we need to delete files if book is digital
      const digitalInfo = await DigitalInfo.findOne({
        where: {
          book_id: bookId
        }
      });
      if (digitalInfo) {
        const {location} = digitalInfo;
        // DELETE FILE
        const filePath = path.normalize(path.resolve(MAIN_PATH, `../public/${location}`));
        await fsPromises.unlink(filePath);
        await DigitalInfo.destroy({
          where: {
            book_id: bookId
          }
        })
      }
      // DELETE BOOK IMAGE
      const imgPath = path.normalize(path.resolve(MAIN_PATH, `../public/${image}`));
      await fsPromises.unlink(imgPath);
      await bookToDelete.destroy();
      res.json({
        success: true,
      })
    } catch (error) {
      next(error)
    }
  };

  async downloadBook(req, res, next) {
    try {
      const bookId = Number(req.params.id);
      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with book id');
      const isBookPresent = await Book.findByPk(bookId);

      if (!isBookPresent) throw new ConflictError('Book was not found');
      const bookInfo = await DigitalInfo.findOne({
        where: {
          book_id: bookId
        }
      });

      if (!bookInfo) throw new ConflictError('Book is not digital');

      res.download(path.join('public', bookInfo.location), bookInfo.location.split('/')[bookInfo.location.split('/').length - 1], function (err) {
        if (err) throw new Error(err);
      });
    } catch (error) {
      next(error);
    }
  };

  /**
   * This us very simple method using for search all books in database
   * @param req
   * @param res
   * @param next
   * @returns booksInfo - all books from dataBase
   */
  async getAllBooks(req, res, next) {
    try {
      const limit = req.query.hasOwnProperty('limit') ? Number(req.query.limit) : 10;
      const page = req.query.hasOwnProperty('page') ? Number(req.query.page) : 1;
      if (typeof limit !== 'number' || isNaN(limit) || limit < 0) throw new ConflictError('Incorrect parameter limit');
      if (typeof page !== 'number' || isNaN(page) || page < 0) throw new ConflictError('Incorrect parameter page');
      const offset = (page * limit) - limit;
      const books = await Book.findAndCountAll({
        order: [
          ['title', 'ASC'],
        ],
        include: [{
          model: Subject,
          attributes: ['title']
        }],
        limit,
        offset
      });
      if (!books && !Array.isArray(books)) throw new ConflictError(`Books were not found`);
      if (books.rows.length === 0) res.json({success: true, books: books.rows});
      const booksIds = books.rows.map(book => book.dataValues.id);
      // SELECT bookid, AVG(star), COUNT(id) FROM rating GROUP BY bookid ORDER BY AVG(star) DESC
      const booksRating = await Rating.findAll({
        attributes: [
          'book_id',
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: 'book_id',
        where: {
          book_id: booksIds
        }
      });
      const allBooks = books.rows.map(book => {
        const rating = booksRating.find(rating => book.id === rating.book_id);
        if (!rating) return book;

        book.dataValues.countOfVotes = rating.dataValues.countOfVotes;
        book.dataValues.avgStar = +(rating.dataValues.avgStar.slice(0, 3));
        return book;
      });
      res.json({
        success: true,
        total: books.count,
        offset: offset,
        books: allBooks
      })
    } catch (error) {
      next(error);
    }
  };

  async getBookById(req, res, next) {
    try {
      const id = Number(req.params.id);
      const limit = req.query.hasOwnProperty('limit') ? Number(req.query.limit) : 10;
      const page = req.query.hasOwnProperty('page') ? Number(req.query.page) : 1;
      if (typeof limit !== 'number' || isNaN(limit) || limit <= 0) throw new ConflictError('Incorrect parameter limit');
      if (typeof page !== 'number' || isNaN(page) || page <= 0) throw new ConflictError('Incorrect parameter page');
      if (typeof id !== 'number' || isNaN(id) || id <= 0) throw new RequestError('Bad request, problem with id');
      const _book = await Book.findOne({
        where: {
          id
        },
        include: [{
          model: Subject,
          attributes: ['title']
        }]
      });
      if (!_book) throw new ConflictError('Book was not found');
      const book = _book.dataValues;
      // SELECT COUNT("comment") FROM comments WHERE bookid = id;
      const offset = (page * limit) - limit;
      const comments = await Comment.findAndCountAll({
        where: {
          book_id: id
        },
        order: [
          ['created_at', 'DESC']
        ],
        include: [User],
        limit,
        offset
      });

      const bookRating = await Rating.findOne({
        attributes: [
          'book_id',
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: 'book_id',
        where: {
          book_id: id
        }
      });
      book.countOfComments = comments.count;
      book.countOfVotes = bookRating ? +bookRating.dataValues.countOfVotes : 0;
      book.avgStar = bookRating ? +(bookRating.dataValues.avgStar.slice(0, 3)) : 0;
      book.comments = comments.rows;
      if (book.is_reading) {
        // SELECT * FROM bookstat b JOIN users u on b.user_id = u.id WHERE b.book_id = id;
        const bookStat = await BookStat.findOne({
          where: {
            book_id: id
          },
          include: [User],
        });
        if (bookStat) {
          book.backTime = bookStat.dataValues.back_time;
          book.userName = bookStat.dataValues.User.name;
          book.userIdWhoRead = bookStat.dataValues.user_id;
          book.is_delaying = bookStat.dataValues.is_delaying;
        }
      }
      res.json({
        success: true,
        book,
      })
    } catch (error) {
      next(error);
    }
  };

  /**
   *
   * @returns top5Book - top 5 book by rating.
   * Firstly we find top 5 books by rating from Rating table.
   * Then we sort this by ID and find all book info from Book table
   * Sort Rating because Book table return sorted by ID array.
   * And because we need to concat two array, we must have a two sorted arrays
   * @param req
   * @param res
   * @param next
   */
  async getTopByComments(req, res, next) {
    try {
      const limit = req.query.hasOwnProperty('limit') ? Number(req.query.limit) : 10;
      const page = req.query.hasOwnProperty('page') ? Number(req.query.page) : 1;
      if (typeof limit !== 'number' || isNaN(limit) || limit < 0) throw new ConflictError('Incorrect parameter limit');
      if (typeof page !== 'number' || isNaN(page) || page < 0) throw new ConflictError('Incorrect parameter page');
      const offset = (page * limit) - limit;

      let topBooksIds = [];
      const commentInfo = await Comment.findAndCountAll({
        attributes: [
          'book_id',
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfComments']
        ],
        order: [[Sequelize.fn('COUNT', Sequelize.col('id')), 'DESC']],
        group: 'book_id',
        limit,
        offset
      });
      commentInfo.rows.forEach(book => {
        topBooksIds.push(book.dataValues.book_id)
      });
      // SELECT bookid, AVG(star), COUNT(id) FROM rating GROUP BY bookid LIMIT 5;
      const ratingInfo = await Rating.findAll({
        attributes: [
          'book_id',
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: 'book_id',
        where: {
          book_id: topBooksIds
        }
      });
      // SELECT * FROM book WHERE id IN top5BooksIds
      const topBooks = await Book.findAll({
        where: {
          id: topBooksIds
        },
        include: [{
          model: Subject,
          attributes: ['title']
        }]
      });
      // timeTester
      const books = topBooks.map((bookStat) => {
        const comment = commentInfo.rows.find(comment => {
          const rating = ratingInfo.find(rating => bookStat.dataValues.id === rating.dataValues.book_id);
          if (!rating) return bookStat.dataValues.id === comment.dataValues.book_id;
          bookStat.dataValues.countOfVotes = +rating.dataValues.countOfVotes;
          bookStat.dataValues.avgStar = +(rating.dataValues.avgStar.slice(0, 3));
          return bookStat.dataValues.id === comment.dataValues.book_id;
        });
        if (!comment) return bookStat;
        bookStat.dataValues.countOfComments = +comment.dataValues.countOfComments;
        return bookStat;
      });

      books.sort((first, second) => {
        return second.dataValues.countOfComments - first.dataValues.countOfComments
      });
      const total = commentInfo.count.length;
      res.json({
        success: true,
        total,
        offset,
        books: books,
      })
    } catch (error) {
      next(error);
    }
  };

  /**
   *
   * @returns top5Book - top 5 book by rating.
   * Firstly we find top 5 books by rating from Rating table.
   * Then we sort this by ID and find all book info from Book table
   * Sort Rating because Book table return sorted by ID array.
   * And because we need to concat two array, we must have a two sorted arrays
   * @param req
   * @param res
   * @param next
   */
  async getTopByRating(req, res, next) {
    try {
      const limit = req.query.hasOwnProperty('limit') ? Number(req.query.limit) : 10;
      const page = req.query.hasOwnProperty('page') ? Number(req.query.page) : 1;
      let topBooksIds = [];
      if (typeof limit !== 'number' || isNaN(limit) || limit < 0) throw new ConflictError('Incorrect parameter limit');
      if (typeof page !== 'number' || isNaN(page) || page < 0) throw new ConflictError('Incorrect parameter page');
      const offsetCount = (page * limit) - limit;
      // SELECT bookid, AVG(star), COUNT(id) FROM rating GROUP BY bookid ORDER BY AVG(star) DESC LIMIT 5 OFFSET 10;
      const booksInfo = await Rating.findAndCountAll({
        attributes: [
          'book_id',
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: 'book_id',
        order: [[Sequelize.fn('AVG', Sequelize.col('star')), 'DESC']],
        limit,
        offset: offsetCount
      });
      booksInfo.rows.forEach(book => {
        topBooksIds.push(book.dataValues.book_id);
      });
      // SELECT * FROM book WHERE id IN top5BooksIds
      const topBooks = await Book.findAll({
        where: {
          id: topBooksIds
        },
        include: [{
          model: Subject,
          attributes: ['title']
        }]
      });
      const books = topBooks.map(bookStat => {
        const rating = booksInfo.rows.find(rating => bookStat.dataValues.id === rating.dataValues.book_id);
        if (!rating) return bookStat;
        bookStat.dataValues.countOfVotes = +rating.dataValues.countOfVotes;
        bookStat.dataValues.avgStar = +(rating.dataValues.avgStar.slice(0, 3));
        return bookStat
      });
      books.sort((first, second) => {
        return second.dataValues.avgStar - first.dataValues.avgStar
      });
      const total = +booksInfo.count.length;
      res.json({
        success: true,
        total,
        offset: offsetCount,
        books: books,
      })
    } catch (error) {
      next(error);
    }
  };

  /**
   *
   * @param req
   * @param res
   * @param next
   * @returns top5Book - top 5 book by rating.
   * Firstly we find top 5 books by rating from Rating table.
   * Then we sort this by ID and find all book info from Book table
   * Sort Rating because Book table return sorted by ID array.
   * And because we need to concat two array, we must have a two sorted arrays
   */
  async getTopByReading(req, res, next) {
    try {
      const limit = req.query.hasOwnProperty('limit') ? Number(req.query.limit) : 10;
      const page = req.query.hasOwnProperty('page') ? Number(req.query.page) : 1;
      if (typeof limit !== 'number' || isNaN(limit) || limit < 0) throw new ConflictError('Incorrect parameter limit');
      if (typeof page !== 'number' || isNaN(page) || page < 0) throw new ConflictError('Incorrect parameter page');
      let topBooksIds = [];
      const offsetCount = (page * limit) - limit;
      const readingInfo = await ReadingActivity.findAndCountAll({
        attributes: [
          'book_id',
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfReading']
        ],
        group: 'book_id',
        order: [[Sequelize.fn('COUNT', Sequelize.col('id')), 'DESC']],
        limit,
        offset: offsetCount,
        where: {
          take_read: true
        }
      });
      readingInfo.rows.forEach(book => {
        topBooksIds.push(book.dataValues.book_id)
      });
      // SELECT bookid, AVG(star), COUNT(id) FROM rating GROUP BY bookid LIMIT 5;
      const ratingInfo = await Rating.findAll({
        attributes: [
          'book_id',
          [Sequelize.fn('AVG', Sequelize.col('star')), 'avgStar'],
          [Sequelize.fn('COUNT', Sequelize.col('id')), 'countOfVotes']
        ],
        group: 'book_id',
        where: {
          book_id: topBooksIds
        }
      });
      // SELECT * FROM book WHERE id IN top5BooksIds
      const topBooks = await Book.findAll({
        where: {
          id: topBooksIds
        },
        include: [{
          model: Subject,
          attributes: ['title']
        }]
      });
      topBooks.map(bookStat => {
        const reading = readingInfo.rows.find(reading => {
          const rating = ratingInfo.forEach(rating => bookStat.id === rating.book_id);
          if (!rating) return bookStat.id === reading.book_id;
          bookStat.dataValues.countOfVotes = rating.dataValues.countOfVotes;
          bookStat.dataValues.avgStar = +(rating.dataValues.avgStar.slice(0, 3));
          return bookStat.id === reading.book_id;
        });
        if (!reading) return bookStat;
        bookStat.dataValues.countOfReading = reading.dataValues.countOfReading;
        return bookStat;
      });
      topBooks.sort((first, second) => second.dataValues.countOfReading - first.dataValues.countOfReading);
      const total = +readingInfo.count.length;

      res.json({
        success: true,
        total,
        offset: offsetCount,
        books: topBooks,
      })
    } catch (error) {
      next(error)
    }
  };

  async returnBookById(req, res, next) {
    try {
      const bookId = Number(req.params.id);
      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with id');
      const {id: userId, role} = req.auth;
      const isBookPresent = await BookStat.findOne({
        where: {
          book_id: bookId
        }
      });
      if (!isBookPresent) throw new Error('Bad request');
      const {user_id} = isBookPresent;
      if (user_id !== userId && !ADMIN_ROLES.includes(role)) throw new Error('You have not permissions');
      await isBookPresent.destroy();
      await Book.update({
        is_reading: false
      }, {
        where: {
          id: bookId
        }
      });
      // Insert record into statistic activity table
      await ReadingActivity.create({
        user_id: userId,
        book_id: bookId,
        get_back: true,
        created_at: new Date().toISOString()
      });
      res.json({
        success: true
      })
    } catch (error) {
      next(error);
    }
  };

  async returnBookByBarcode(req, res, next) {
    try {
      const {barcode} = req.params;
      if (typeof barcode !== 'string' || !barcode) throw new RequestError('Bad request, problem with barcode');
      const {id: userId, role} = req.auth;
      const book = await Book.findOne({
        where: {
          barcode,
        }
      });
      const {id: bookId} = book;
      const bookStat = await BookStat.findOne({
        where: {
          book_id: bookId
        }
      });
      if (!bookStat) throw new ConflictError('Problem with barcode don`t match with book');
      const {user_id} = bookStat;
      if (user_id !== userId && !ADMIN_ROLES.includes(role)) throw new Error('You have not permissions');
      await bookStat.destroy();
      await book.update({
        is_reading: false
      });
      // Insert record into statistic activity table
      await ReadingActivity.create({
        user_id: userId,
        book_id: bookId,
        get_back: true,
        created_at: new Date().toISOString()
      });
      res.json({
        success: true
      })
    } catch (error) {
      next(error)
    }
  }

  async takeBookForReadingByBarcode(req, res, next) {
    try {
      const barcode = req.params.barcode;
      if (!barcode || typeof barcode !== 'string') throw new RequestError('Bad request, problem with barcode');
      const {id: userId} = req.auth;
      let book = await Book.findOne({
        where: {
          barcode: barcode
        }
      });
      let {id: bookId} = book;
      let bookStatCheck = await BookStat.findOne({
        where: {
          user_id: userId,
          book_id: bookId
        }
      });

      if (book.dataValues.is_reading && bookStatCheck) res.json({
        success: true,
        message: 'You are reading the book'
      });
      if (book.dataValues.is_reading) res.json({
        success: false,
        message: 'Your colleague is reading the book'
      });
      const backTime = new Date(Date.now() + MILLISECONDS_ID_DAY * 31).toISOString();
      await book.update({
        is_reading: true
      });

      await BookStat.create({
        book_id: bookId,
        user_id: userId,
        get_time: new Date().toISOString(),
        back_time: backTime
      });

      ReadingActivity.create({
        user_id: userId,
        book_id: bookId,
        take_read: true,
        created_at: new Date().toISOString()
      });
      res.json({
        success: true
      })
    } catch (error) {
      next(error)
    }


  };

  async takeBookForReadingById(req, res, next) {
    try {
      const bookId = Number(req.params.id);
      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with book id');
      const {id: userId} = req.auth;
      let book = await Book.findByPk(bookId);
      let bookStatCheck = await BookStat.findOne({
        where: {
          book_id: bookId,
          user_id: userId
        }
      });
      if (book.dataValues.is_reading && bookStatCheck) res.json({
        success: false,
        message: 'You are reading the book'
      });
      if (book.dataValues.is_reading) res.json({
        success: false,
        message: 'Your colleague is reading the book'
      });
      const backTime = new Date(Date.now() + MILLISECONDS_ID_DAY * 31).toISOString();
      await book.update({
        is_reading: true,
      });
      await BookStat.create({
        book_id: bookId,
        user_id: userId,
        get_time: new Date().toISOString(),
        back_time: backTime
      });
      // Insert record into statistic activity table
      await ReadingActivity.create({
        user_id: userId,
        book_id: bookId,
        take_read: true,
        created_at: new Date().toISOString()
      });
      res.json({
        success: true
      })
    } catch (error) {
      next(error)
    }
  };

  /**
   * This method using for update book with new parameters
   * Firstly we need to find book in database
   * Then we get new values from request
   * If new value is empty we take old value
   * If we have files, then we delete old files
   * @param {Object}req
   * @param {Object}res
   * @param {Function}next
   * @returns {Promise<void>}
   */
  async updateBook(req, res, next) {
    try {
      const schema = Joi.object().keys({
        title: Joi.string().alphanum().min(3),
        author: Joi.string().alphanum().min(2),
        summary: Joi.string().alphanum(),
        subject: Joi.string().alphanum(),
        publisher: Joi.string().alphanum(),
        tags: Joi.string().alphanum(),
        typeOfBook: Joi.string().alphanum(),
        // barcode: Joi.string().alphanum().min(8)
      });
      const bookInfo = Joi.validate(req.body, schema);
      const bookId = Number(req.params.id);
      if (bookInfo.error) throw new RequestError('Bad request, check data');
      if (typeof bookId !== 'number' || isNaN(bookId) || bookId <= 0) throw new RequestError('Bad request, problem with book id');

      let {photo = '', file = ''} = req.files;
      let fileInfo;
      if (file) [fileInfo] = file;

      const book = await Book.findByPk(bookId);
      if (!book) throw new ConflictError('Book was not found');
      let {
        title: newTitle,
        author: newAuthor,
        summary: newSummary,
        subject: newSubject,
        publisher: newPublisher,
        tags: newTags,
        typeOfBook
      } = bookInfo.value;

      let {id: newSubject_id} = await Subject.findOne({
        where: {
          title: newSubject
        }
      });
      if (!newSubject_id) {
        let newSubject = await Subject.create({
          title: newSubject
        });
        newSubject_id = newSubject.dataValues.id
      }

      let {
        title: oldTitle,
        author: oldAuthor,
        summary: oldSummary,
        tags: oldTags,
        subject_id: oldSubject_id,
        publisher: oldPublisher = '',
        image
      } = book;

      if (photo) {
        photo = `/images/${photo[0].filename}`;
        const filePath = path.normalize(path.resolve(MAIN_PATH, `../public/${image}`));
        await fsPromises.unlink(filePath);
      } else {
        photo = image;
      }

      const infoToUpdate = {
        title: newTitle ? newTitle : oldTitle,
        author: newAuthor ? newAuthor : oldAuthor,
        summary: newSummary ? newSummary : oldSummary,
        subject_id: newSubject_id ? newSubject_id : oldSubject_id,
        tags: newTags ? newTags.toLowerCase() : oldTags,
        is_digital: typeOfBook === 'digital',
        publisher: newPublisher ? newPublisher : oldPublisher,
        image: photo
      };

      updateAuxiliary(bookId, infoToUpdate, fileInfo);

      await book.update(infoToUpdate);

      res.json({
        success: true
      });
    } catch (error) {
      next(error);
    }
  };
}

module.exports = new Books();