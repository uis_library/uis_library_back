const { RequestError } = require('../errors');

module.exports = (err, req, res, next) => {
  switch (err.name) {
    case 'ValidationError':
      return next(new RequestError(err.message));

    default:
      return next(err);
  }
};
