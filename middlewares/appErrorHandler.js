const {AppError} = require('../errors');

module.exports = (err, req, res, next) => {
    if (!(err instanceof AppError)) {
      return res.status(err.status || 500).json({ success: false, error: err.message});
    }

    const {message, status, stack, details} = err;
    res.status(status).json({success: false, error: {message, details}});
};
