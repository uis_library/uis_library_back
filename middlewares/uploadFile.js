const {IMAGES, AUDIO, TEXT, VIDEO} = require('../constants/files');
const multer = require('multer');

const storage = multer.diskStorage({
    // it folder where we store file
    destination: function (req, file, cb) {
        try {
            if (IMAGES.includes(file.mimetype)) {
              cb(null, 'public/images')
            } else if (AUDIO.includes(file.mimetype) || TEXT.includes(file.mimetype)) {
                cb(null, 'public/files')
            } else {
                cb('UNKNOWN FILE', null);
            }
        } catch (e) {
            console.log(e.message)
        }
    },
    //
    filename: function (req, file, cb) {
        try {
            // Build name like 1549350785625.jpg
            cb(null, new Date().getTime() + '.' + file.originalname.split('.').pop())
        } catch (e) {
            console.log(e.message);
        }
    }
});
module.exports = multer({storage});
