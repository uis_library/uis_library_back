const db = require('../models');
const {Auth_access} = db.sequelize.models;
const token = require('../helpers/token');
const authConfig = require('../config/authentication');
const {AuthError} = require('../errors');

module.exports = async (req, res, next) => {
    try {
        if (!req.headers.hasOwnProperty('authorization')) throw new AuthError('User is not authorized');

        let accessToken = req.headers.authorization;
        accessToken = accessToken.substr(7);

        const isExistsToken = await Auth_access.findOne({
           where : {
               token: accessToken
           }
        });

        if (!isExistsToken) throw new AuthError('User was not found');

        const credentials = await token.verifyToken(accessToken, authConfig.getSecret());
        req.auth = {...credentials, accessToken};
        next();
    } catch (err) {
        next(err)
    }
};
