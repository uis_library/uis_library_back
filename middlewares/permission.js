const {ForbiddenError} = require('../errors');

module.exports = (...allowed) => {
    const isAllowed = role => allowed.includes(role);
    return (req, res, next) => {
        console.log('Permission\n');
        if (!(req.auth && isAllowed(req.auth.role))) throw new ForbiddenError();
        next(); // role is allowed, so continue on the next middleware
    }
};
