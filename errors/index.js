class AppError extends Error {
  /**
   *
   * @param {String} message
   * @param {Object?} details
   */
  constructor(message, details = null) {
    super(message);

    this.details = details;
    this.name = 'AppError';
  }
}

class RequestError extends AppError {
  constructor(message = 'Bad request', details) {
    super(message, details);

    this.name = 'RequestError';
    this.status = 400;
  }
}

class ResourceError extends AppError {
  constructor(message = 'Resource not found', details) {
    super(message, details);

    this.name = 'ResourceError';
    this.status = 404;
  }
}

class ConflictError extends AppError {
  constructor(message = 'Conflict', details) {
    super(message, details);

    this.name = 'ConflictError';
    this.status = 409;
  }
}

class AuthError extends AppError {
  constructor(message = 'Unauthorized', details) {
    super(message, details);

    this.name = 'AuthError';
    this.status = 401;
  }
}

class ForbiddenError extends AppError {
  constructor(message = 'Forbidden', details) {
    super(message, details);

    this.name = 'Forbidden';
    this.status = 403;
  }
}

module.exports = {
  AppError,
  RequestError,
  ResourceError,
  ConflictError,
  AuthError,
  ForbiddenError,
};
